module gitlab.com/robogicu/brain

go 1.14

require (
	github.com/d2r2/go-i2c v0.0.0-20191123181816-73a8a799d6bc
	github.com/d2r2/go-logger v0.0.0-20181221090742-9998a510495e
	github.com/davecgh/go-spew v1.1.1
	github.com/dhowden/raspicam v0.0.0-20190323051945-60ef25a6629f
	github.com/hybridgroup/mjpeg v0.0.0-20140228234708-4680f319790e
	github.com/mattn/go-mjpeg v0.0.0-20180621123927-a9cedbfd1cd8
	github.com/octago/sflags v0.2.0
	go.etcd.io/bbolt v1.3.5
	go.uber.org/atomic v1.6.0
	go.uber.org/multierr v1.5.0
	go.uber.org/zap v1.15.0
	gocv.io/x/gocv v0.23.0
	golang.org/x/sys v0.0.0-20200622214017-ed371f2e16b4
)
