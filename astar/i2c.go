package astar

import "time"

// I2CRead a number of bytes from addr into buf using i2c
// and return the buffer after it is done
func (a AStar) readI2C(reg byte, buf []byte) ([]byte, error) {

	a.mux.Lock()
	defer a.mux.Unlock()

	// write on addr to get buttons
	_, err := a.I2C.WriteBytes([]byte{reg})
	if err != nil {
		return buf, err
	}

	// sleep a bit because the the microcontroller on astar can't switch that fast
	// between read and write (1ms works too)
	time.Sleep(time.Millisecond * 3)

	// get button status from gicu
	_, err = a.I2C.ReadBytes(buf)
	if err != nil {
		return buf, err
	}

	return buf, nil
}
