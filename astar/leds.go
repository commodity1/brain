package astar

import (
	"fmt"
	"gitlab.com/robogicu/brain/utils"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"time"
)

func (a AStar) TestLed() {
	var err error

	zap.S().Info("testing leds: all on, all off, 5xcross")

	// all on
	for i := 1; i < 4; i++ {
		if err = a.TurnOnLed(i); err != nil {
			zap.S().Errorf("cannot turn off led %v: %v", i, err)
			return
		}
	}

	time.Sleep(time.Second)

	// all off
	for i := 1; i < 4; i++ {
		if err = a.TurnOffLed(i); err != nil {
			zap.S().Errorf("cannot turn on led %v: %v", i, err)
			return
		}
	}

	// cross 5 times
	for c := 0; c < 5; c++ {
		for i := 1; i < 4; i++ {
			if err = a.TurnOnLed(i); err != nil {
				zap.S().Errorf("cannot turn on-cross led %v: %v", i, err)
				return
			}
			time.Sleep(time.Second / 2)
			if err = a.TurnOffLed(i); err != nil {
				zap.S().Errorf("cannot turn off-cross led %v: %v", i, err)
				return
			}
			time.Sleep(time.Second / 2)
		}
	}
	zap.S().Info("finished testing leds")
}

// ToggleLed toggles a led on and off
func (a AStar) ToggleLed(l int) error {

	// get current state
	state := make([]byte, 1)
	key := fmt.Sprintf("led_%v", l)
	err := a.db.View(func(tx *bolt.Tx) error {
		s := tx.Bucket([]byte(SensorBucket)).Get([]byte(key))
		copy(state, s)
		return nil
	})

	if err != nil {
		return err
	}

	// get current state (as bool) and rewrite the state
	bstate := utils.ByteToBool(state[0])
	err = a.writeLedState(l, !bstate)
	if err != nil {
		return err
	}

	return nil
}

func (a AStar) TurnOnLed(l int) error {
	return a.writeLedState(l, true)
}

func (a AStar) TurnOffLed(l int) error {
	return a.writeLedState(l, false)
}

// updateDBLeds updates the state of the leds in BoltDB
func (a AStar) updateDBLeds(bucket *bolt.Bucket, buf []byte) error {
	for i := 1; i < 4; i++ {
		key := fmt.Sprintf("led_%d", i)
		err := bucket.Put([]byte(key), []byte{buf[i-1]})
		if err != nil {
			return fmt.Errorf("could not update led %v: %v", i, err)
		}
	}
	return nil
}

func (a AStar) writeLedState(l int, state bool) error {
	a.mux.Lock()
	defer a.mux.Unlock()

	s := utils.BoolToByte(state)
	buf := []byte{byte(l - 1), s}
	_, err := a.I2C.WriteBytes(buf)

	if err != nil {
		return err
	}
	return nil
}
