package astar

import (
	"fmt"
	bolt "go.etcd.io/bbolt"
)

// updateDBMotors updates the state of the motors in BoltDB
func (a AStar) updateDBMotors(bucket *bolt.Bucket, buf []byte) error {
	err := bucket.Put([]byte("motor_left"), buf[6:8])
	if err != nil {
		return fmt.Errorf("could not update left motor: %v", err)
	}
	err = bucket.Put([]byte("motor_right"), buf[8:10])
	if err != nil {
		return fmt.Errorf("could not update right motor: %v", err)
	}
	return nil
}
