package astar

import (
	"fmt"
	bolt "go.etcd.io/bbolt"
	"time"
)

// WatchButton gets the button by id (b) and for each change of the value from 0 to 1
// will execute fns from f
func (a AStar) WatchButton(b int, f []func()) {
	prev := make([]byte, 1)
	for {
		err := a.db.View(func(tx *bolt.Tx) error {
			key := fmt.Sprintf("button_%d", b)
			state := tx.Bucket([]byte(SensorBucket)).Get([]byte(key))
			if state == nil {
				return nil
			}
			if state[0] == 1 {
				if prev[0] == 0 {
					prev[0] = 1
					for _, fn := range f {
						go fn()
					}
				}
			} else {
				prev[0] = 0
			}
			return nil
		})
		if err != nil {
			continue
		}
		time.Sleep(time.Millisecond * 100)
	}
}

// updateDBButtons updates the state of the buttons in BoltDB
func (a AStar) updateDBButtons(bucket *bolt.Bucket, buf []byte) error {
	// update buttons (bytes 3, 4, 5)
	for i := 1; i < 4; i++ {
		key := fmt.Sprintf("button_%d", i)
		err := bucket.Put([]byte(key), []byte{buf[3+i-1]})
		if err != nil {
			return fmt.Errorf("could not update button %v: %v", i, err)
		}
	}
	return nil
}
