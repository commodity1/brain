package astar

import (
	"bytes"
	"context"
	"github.com/d2r2/go-i2c"
	"gitlab.com/robogicu/brain/db"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"sync"
	"time"
)

const (
	SensorBucket = "sensor"
)

type AStar struct {
	I2C  *i2c.I2C
	db   *bolt.DB
	mux  *sync.Mutex
	buf  []byte
	size int

	ctx context.Context
	ec  chan error
}

// NewAStar creates a new AStar object
func NewAStar(ctx context.Context, i2c *i2c.I2C, db *bolt.DB, size int, ec chan error) *AStar {

	a := &AStar{I2C: i2c, db: db, ctx: ctx, ec: ec, size: size}
	a.mux = &sync.Mutex{}
	a.buf = make([]byte, size)
	return a
}

// SyncDB refreshes the information from AStar via I2C (every t) and in case it has been changed
// it will update BoltDB by calling updateDB()
func (a AStar) SyncDB(t time.Duration) {

	prev := make([]byte, a.size)
	tick := time.Tick(t)

	for {
		select {
		case <-tick:
			_, err := a.readI2C(0x0, a.buf)
			if err != nil {
				a.ec <- err
				continue
			}
			if !bytes.Equal(a.buf, prev) {
				err = a.updateDB(a.buf)
				if err != nil {
					a.ec <- err
					continue
				}
				copy(prev, a.buf)
			}
		case <-a.ctx.Done():
			return
		}
	}

}

// updateDB gets the buffer from SyncDB and updates BoltDB with buffer contents
func (a AStar) updateDB(buf []byte) error {

	// write the buffer to the db
	err := a.db.Batch(func(tx *bolt.Tx) error {
		var err error
		bucket := tx.Bucket([]byte(SensorBucket))

		// update leds
		err = a.updateDBLeds(bucket, buf)
		if err != nil {
			return err
		}

		// update buttons
		err = a.updateDBButtons(bucket, buf)
		if err != nil {
			return err
		}

		// update motors
		err = a.updateDBMotors(bucket, buf)
		if err != nil {
			return err
		}

		// battery mV
		err = a.updateDBBattery(bucket, buf)
		if err != nil {
			return nil
		}

		// raw buffer to DB
		err = bucket.Put([]byte("raw"), buf)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

func (a AStar) DumpDB() {
	err := db.DumpBucket(a.db, "sensor")
	if err != nil {
		zap.S().Errorf("cannot dump db: %v", err)
	}
}
