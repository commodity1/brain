package astar

import (
	"encoding/binary"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"time"
)

const (
	MaxBatteryMilliV = 8300
	MinBatteryMilliV = 7200
)

// GetBatteryLevel returns the current level of battery as reported by Astar
func (a AStar) GetBatteryLevel() (uint16, error) {
	lvl := make([]byte, 2)
	err := a.db.View(func(tx *bolt.Tx) error {
		state := tx.Bucket([]byte(SensorBucket)).Get([]byte("battery"))
		copy(lvl, state)
		return nil
	})
	if err != nil {
		return 0, err
	}
	return binary.LittleEndian.Uint16(lvl), nil
}

func (a AStar) updateDBBattery(bucket *bolt.Bucket, buf []byte) error {
	err := bucket.Put([]byte("battery"), buf[10:12])
	if err != nil {
		return err
	}

	return nil
}

// BatteryButton logs the current level of battery
func (a AStar) BatteryButton() {
	lvl, err := a.GetBatteryLevel()
	if err != nil {
		zap.S().Infof("could not read battery level: %v", err)
	}
	perc := ((float32(lvl) - MinBatteryMilliV) * 100) / (MaxBatteryMilliV - MinBatteryMilliV)
	if perc > 0 {
		zap.S().Infof("battery level is: %.3fV (%.2f%%)", float32(lvl)/1000, perc)
	} else {
		zap.S().Info("cannot read battery level, connected via USB")
	}

}

// BatteryButton logs the current level of battery
func (a AStar) BatteryLogger() {
	tick := time.Tick(5 * time.Second)

	for {
		select {
		case <-a.ctx.Done():
			return
		case <-tick:
			a.BatteryButton()
		}
	}

}
