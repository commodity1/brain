package utils

import "context"

type ContextBag struct {
	Astar  ContextPair
	Camera ContextPair
	Stream ContextPair
	HTTP   ContextPair
}

type ContextPair struct {
	Context context.Context
	Cancel  func()
}
