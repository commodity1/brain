package utils

// ByteToBool returns false in case b is 0x0 else returns true
func ByteToBool(b byte) bool {
	if b == 0x0 {
		return false
	}

	return true
}

func BoolToByte(b bool) byte {
	if b {
		return 0x1
	}
	return 0x0
}

func BoolByteToString(b byte) string {
	if bl := ByteToBool(b); bl {
		return "on"
	} else {
		return "off"
	}

}
