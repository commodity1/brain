package program

import (
	"image"
	"os"
)

type ProgramManager struct {
	ic chan image.Image
	qc chan os.Signal
}

func NewProgramManager(qc chan os.Signal) *ProgramManager {
	return &ProgramManager{qc: qc}
}
