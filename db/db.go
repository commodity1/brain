package db

import (
	"encoding/json"
	"fmt"
	"gitlab.com/robogicu/brain/utils"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
)

const (
	SensorBucket = "sensor"
)

// setupDB create a new bolt database and creates the buckets
// required by the app to run
func SetupDB(path string) (*bolt.DB, error) {
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		return nil, fmt.Errorf("could not open db: %v", err)
	}

	err = db.Update(func(tx *bolt.Tx) error {

		_, err = tx.CreateBucketIfNotExists([]byte(SensorBucket))
		if err != nil {
			return fmt.Errorf("could not create sensor bucket: %v", err)
		}

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("could not setup db: %v", err)
	}
	return db, nil
}

func DumpBucket(db *bolt.DB, bucket string) error {
	output := make(map[string]string)
	err := db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket([]byte(bucket)).Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			switch len(v) {
			case 1:
				output[string(k)] = utils.BoolByteToString(v[0])
			}

		}
		b, err := json.Marshal(output)
		if err != nil {
			return err
		}
		zap.S().Infof("current db: %v", string(b))
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}
